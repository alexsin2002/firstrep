//
//  ArrayViewController.swift
//  twentyNine
//
//  Created by Синицын on 20.12.2018.
//  Copyright © 2018 Синицын. All rights reserved.
//

import UIKit

class ArrayViewController: UIViewController {
    
    //@IBOutlet используется для связывания элементов интерфейса с кодом
    @IBOutlet weak var myLabel: UILabel!
    
    //@IBOutlet используется для связывания элементов интерфейса с кодом
    @IBOutlet weak var myTextFieldNumberElement: UITextField!
    
    //@IBOutlet используется для связывания элементов интерфейса с кодом
    @IBOutlet weak var myTextField: UITextField!
    
    //Объявление массива someString типа String
    var someString = [String]()
    
    //объявляется функция buttonPush типа IBOAction, которая дает возможность делать связь с кодом по действию
    @IBAction func buttonPush() {
        
        //переменной myTextFieldText присваевается знаенеие хранящееся в myTextField
        if let myTextFieldText = myTextField.text {
            
            //объявляется переменная isContained которой присваевается значение функции contained, у которой есть возвращаемый аргумент типа Bool, следовательно переменной isContained присваевается значение true, либо false
            let isContained = contained(word: myTextFieldText)
            
            
            //если значение true, то выводится в label текст: "Это слово уже есть"
            if isContained {
                myLabel.text = "Это слово уже есть"
                
                //в противном случае в массив someString добавляется слово
            } else {
                someString.append(myTextFieldText)
            }
        }
    }
    
    @IBAction func buttonPull() {
        
        //присваивание numberItem, то что находится в myTextFieldNumberElement, если в нём ничего нет, то присваивается пустая строка
        let numberItem = myTextFieldNumberElement.text ?? ""
        
        //приведение типа String в тип Int. Переменная fNumberItem хранит в себе номер элемента (item)
        if let fNumberItem = Int(numberItem) {
        
            //если значение переменной fNumberItem (которая хранит номер item) меньше чем item в массиве someString, тогда выполняются следующие действия
            if fNumberItem < someString.count {
                
                //объявляется переменнная item, в ней находится номер item хранящийся в массиве по числу котрое хранится в переменной fNumberItem
                var item = someString[fNumberItem]
                
                //в lable выводится номер item
                myLabel.text = item
                
                //в противном случае выводится в label текст: "Такого item нет в массиве, попробуй другой item"
            } else {
                myLabel.text = "Такого item нет в массиве, попробуй другой item"
            }
        }
    }
    
    //функция contained нужна для проверки совпадения слов хранящиеся в item со словом которое хотят добавить в массив
    
    //объявляется функция с аргументами типа String, тип возвращаемого аргумента Bool
    func contained(word: String) -> Bool {
        
        //объявляется цикл for который проверяет все item в массиве someString
        for item in someString {
            
            //если word совпадает с item, то возвращается значение true
            if word == item {
                return true
            }
        }
        //если нет, то возвращается false
        return false
    }
    
}
    


